#![feature(async_await)]

use goa::*;

#[tokio::main]
async fn main() -> Result<(), Error> {
    let goa_runtime = Runtime::new();
    let client = goa_runtime.client().await?;
    let accounts = client.accounts();

    println!("{:?}", client);

    for account in accounts {
        println!("\n{:#?}\n", account);
        if let Some(mail) = account.mail() {
            println!("\n{:#?}\n", mail);
        }
        if let Some(auth) = account.oauth_based() {
            println!("\n{:#?}\n", auth);
        }
        if let Some(auth) = account.oauth2_based() {
            println!("\n{:#?}\n", auth);
        }
        if let Some(auth) = account.password_based() {
            println!("\n{:#?}\n", auth);
            println!("IMAP PASSWORD: {:?}", auth.imap_password());
            println!("SMTP PASSWORD: {:?}", auth.smtp_password());
        }
    }

    Ok(())
}
