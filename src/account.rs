mod auth {
    mod oauth;
    mod oauth2;
    mod password;
    pub use {oauth::OAuthBased, oauth2::OAuth2Based, password::PasswordBased};
}
mod mail;

use crate::ffi::{
    goa_account_get_identity, goa_account_get_mail_disabled, goa_account_get_presentation_identity,
    goa_account_get_provider_icon, goa_account_get_provider_name, goa_account_get_provider_type,
    GoaAccount, GoaObject, GoaObjectIface,
};
use crate::utils::char_ptr_to_string;
pub use auth::{OAuth2Based, OAuthBased, PasswordBased};
use glib::glib_sys::GTRUE;
pub use mail::Mail;
use std::{fmt, marker::PhantomData, ptr::NonNull};

pub struct Account<'a> {
    object: NonNull<GoaObject>,
    account: NonNull<GoaAccount>,
    _marker: PhantomData<&'a ()>,
}

impl Account<'_> {
    /// Returns `None` if `object` is not a valid GoaAccount.
    pub(crate) fn new(object: NonNull<GoaObject>) -> Option<Self> {
        object.as_goa_account().map(|account| Account {
            object,
            account,
            _marker: PhantomData,
        })
    }

    /// The human-readable name of the provider.
    ///
    /// *e.g.* “Google” or “Microsoft”, for example.
    #[inline]
    pub fn provider_name(&self) -> Option<String> {
        unsafe { char_ptr_to_string(goa_account_get_provider_name(self.account.as_ptr())) }
    }

    /// The human-readable string that identifies uniquely the account at the provider.
    ///
    /// *e.g.* “clientname@gmail.com” for a google account.
    #[inline]
    pub fn presentation_identity(&self) -> Option<String> {
        unsafe { char_ptr_to_string(goa_account_get_presentation_identity(self.account.as_ptr())) }
    }

    /// The serialized icon/logo of the provider.
    ///
    /// Use gio::Icon::new_for_string to recover the icon.
    #[inline]
    pub fn provider_icon(&self) -> Option<String> {
        unsafe { char_ptr_to_string(goa_account_get_provider_icon(self.account.as_ptr())) }
    }

    #[inline]
    pub fn provider_type(&self) -> Option<String> {
        unsafe { char_ptr_to_string(goa_account_get_provider_type(self.account.as_ptr())) }
    }

    #[inline]
    pub fn identity(&self) -> Option<String> {
        unsafe { char_ptr_to_string(goa_account_get_identity(self.account.as_ptr())) }
    }

    #[inline]
    pub fn mail_disabled(&self) -> bool {
        let disabled = unsafe { goa_account_get_mail_disabled(self.account.as_ptr()) };

        disabled == GTRUE
    }

    pub fn mail(&self) -> Option<Mail> {
        Mail::new(self.object)
    }

    pub fn oauth_based(&self) -> Option<OAuthBased> {
        OAuthBased::new(self.object)
    }

    pub fn oauth2_based(&self) -> Option<OAuth2Based> {
        OAuth2Based::new(self.object)
    }

    pub fn password_based(&self) -> Option<PasswordBased> {
        PasswordBased::new(self.object)
    }
}

impl fmt::Debug for Account<'_> {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        f.debug_struct("Account")
            .field("provider_name", &self.provider_name())
            .field("presentation_identity", &self.presentation_identity())
            .field("provider_type", &self.provider_type())
            .field("identity", &self.identity())
            .field("mail_disabled", &self.mail_disabled())
            .finish()
    }
}
