use crate::{client::future_client::FutureClient, Error, Client};
use glib::MainLoop;
use std::{
    future::Future,
    ops::Not,
    sync::Arc,
    thread::{sleep, spawn, JoinHandle},
    time::Duration,
};

pub struct Runtime {
    main_loop: Arc<MainLoop>,
    loop_handle: Option<JoinHandle<()>>,
}

impl Runtime {
    pub fn new() -> Self {
        let main_loop = Arc::new(MainLoop::new(None, false));
        let shared_main_loop = Arc::clone(&main_loop);
        let loop_handle = spawn(move || shared_main_loop.run());

        Runtime {
            main_loop,
            loop_handle: Some(loop_handle),
        }
    }

    /// Returns asynchronously a client.
    pub fn client<'a>(&'a self) -> impl Future<Output = Result<Client<'a>, Error>> + 'a {
        FutureClient::new()
    }
}

impl Drop for Runtime {
    fn drop(&mut self) {
        if let Some(handle) = self.loop_handle.take() {
            println!("Droppin' main loop…");
            while self.main_loop.is_running().not() {
                // This loop is super important. The `quit` function MUST be called
                // AFTER the `run` function, otherwise the join will lock.
                sleep(Duration::from_millis(1));
            }
            self.main_loop.quit();

            println!("Droppin' handle…");
            match handle.join() {
                Err(_) => eprintln!("ERROR: cannot join the GLib main loop"),
                Ok(_main_loop) => (), //TODO: what should I do with it?
            }
        }
    }
}
