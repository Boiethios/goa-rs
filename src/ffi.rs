use glib::{
    glib_sys::{gboolean, GError, GList},
    gobject_sys::GObject,
};
use std::{
    ffi::c_void,
    marker::PhantomData,
    os::raw::{c_char, c_int},
    ptr::NonNull,
};

macro_rules! extern_type {
    ( $name:ident ) => {
        #[repr(C)]
        pub struct $name {
            _private: [u8; 0],
        }
    };
}

extern_type!(GoaObject);
extern_type!(GoaAccount);
extern_type!(GoaClient);
extern_type!(GoaMail);
extern_type!(GoaOAuthBased);
extern_type!(GoaOAuth2Based);
extern_type!(GoaPasswordBased);

extern_type!(GAsyncResult);
extern_type!(GCancellable);

pub type GAsyncReadyCallback = unsafe extern "C" fn(
    source_object: *mut GObject,
    res: *mut GAsyncResult,
    user_data: *mut c_void,
);

#[allow(unused)] //TMP
extern "C" {
    /// Asynchronously gets a GoaClient. When the operation is finished,
    /// callback will be invoked in the thread-default main loop
    /// of the thread you are calling this method from.
    pub fn goa_client_new(
        cancellable: *mut GCancellable,
        callback: GAsyncReadyCallback,
        user_data: *mut c_void,
    );
    pub fn goa_client_new_finish(res: *mut GAsyncResult, error: *mut *mut GError)
        -> *mut GoaClient;

    pub fn goa_account_call_ensure_credentials(
        proxy: *mut GoaAccount,
        cancellable: *mut GCancellable,
        callback: GAsyncReadyCallback,
        user_data: *mut c_void,
    );
    pub fn goa_account_call_ensure_credentials_finish(
        proxy: *mut GoaAccount,
        out_expires_in: *mut c_int,
        res: *mut GAsyncResult,
        error: *mut *mut GError,
    ) -> gboolean;

    pub fn goa_password_based_call_get_password(
        proxy: *mut GoaPasswordBased,
        arg_id: *const c_char,
        cancellable: *mut GCancellable,
        callback: GAsyncReadyCallback,
        user_data: *mut c_void,
    );
    pub fn goa_password_based_call_get_password_finish(
        proxy: *mut GoaPasswordBased,
        out_password: *mut *mut c_char,
        res: *mut GAsyncResult,
        error: *mut *mut GError,
    ) -> gboolean;

    pub fn goa_client_get_accounts(client: *mut GoaClient) -> *mut GList;

    pub fn goa_object_get_account(object: *mut GoaObject) -> *mut GoaAccount;
    pub fn goa_object_get_mail(object: *mut GoaObject) -> *mut GoaMail;
    pub fn goa_object_get_oauth_based(object: *mut GoaObject) -> *mut GoaOAuthBased;
    pub fn goa_object_get_oauth2_based(object: *mut GoaObject) -> *mut GoaOAuth2Based;
    pub fn goa_object_get_password_based(object: *mut GoaObject) -> *mut GoaPasswordBased;

    pub fn goa_account_get_provider_name(object: *mut GoaAccount) -> *const c_char;
    pub fn goa_account_get_provider_type(object: *mut GoaAccount) -> *const c_char;
    pub fn goa_account_get_identity(object: *mut GoaAccount) -> *const c_char;
    pub fn goa_account_get_presentation_identity(object: *mut GoaAccount) -> *const c_char;
    pub fn goa_account_get_provider_icon(object: *mut GoaAccount) -> *const c_char;
    pub fn goa_account_get_mail_disabled(object: *mut GoaAccount) -> gboolean;

    pub fn goa_mail_get_email_address(object: *mut GoaMail) -> *const c_char;
    pub fn goa_mail_get_name(object: *mut GoaMail) -> *const c_char;
    pub fn goa_mail_get_imap_user_name(object: *mut GoaMail) -> *const c_char;
    pub fn goa_mail_get_imap_host(object: *mut GoaMail) -> *const c_char;
    pub fn goa_mail_get_imap_supported(object: *mut GoaMail) -> gboolean;
    pub fn goa_mail_get_imap_accept_ssl_errors(object: *mut GoaMail) -> gboolean;
    pub fn goa_mail_get_imap_use_ssl(object: *mut GoaMail) -> gboolean;
    pub fn goa_mail_get_imap_use_tls(object: *mut GoaMail) -> gboolean;

    pub fn goa_mail_get_smtp_host(object: *mut GoaMail) -> *const c_char;
    pub fn goa_mail_get_smtp_user_name(object: *mut GoaMail) -> *const c_char;
    pub fn goa_mail_get_smtp_supported(object: *mut GoaMail) -> gboolean;
    pub fn goa_mail_get_smtp_accept_ssl_errors(object: *mut GoaMail) -> gboolean;
    pub fn goa_mail_get_smtp_use_auth(object: *mut GoaMail) -> gboolean;
    pub fn goa_mail_get_smtp_auth_login(object: *mut GoaMail) -> gboolean;
    pub fn goa_mail_get_smtp_auth_plain(object: *mut GoaMail) -> gboolean;
    pub fn goa_mail_get_smtp_auth_xoauth2(object: *mut GoaMail) -> gboolean;
    pub fn goa_mail_get_smtp_use_ssl(object: *mut GoaMail) -> gboolean;
    pub fn goa_mail_get_smtp_use_tls(object: *mut GoaMail) -> gboolean;
    /*
    void 	    goa_account_call_ensure_credentials ()
    gboolean 	goa_account_call_ensure_credentials_finish ()
    gboolean 	goa_account_call_ensure_credentials_sync ()
    void 	    goa_account_complete_ensure_credentials ()
    */

    // TMP
    pub fn goa_password_based_call_get_password_sync(
        proxy: *mut GoaPasswordBased,
        arg_id: *const c_char,
        out_password: *mut *mut c_char,
        cancellable: *mut GCancellable,
        error: *mut *mut GError,
    ) -> gboolean;
}

/// Allows to iterate over a GList correctly and as safely as possible.
pub struct GListIter<T> {
    glist: Option<NonNull<GList>>,
    _marker: PhantomData<T>,
}

impl<T> GListIter<T> {
    pub fn new(glist: *mut GList) -> Self {
        GListIter {
            glist: NonNull::new(glist),
            _marker: PhantomData,
        }
    }
}

impl<T> Iterator for GListIter<T> {
    type Item = NonNull<T>;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(current) = self.glist {
            let data = unsafe { (*current.as_ptr()).data as *mut T };

            self.glist = NonNull::new(unsafe { (*current.as_ptr()).next });
            //TODO: don't end the iterator when a data ptr is null
            NonNull::new(data)
        } else {
            None
        }
    }
}

pub trait GoaObjectIface {
    fn as_goa_account(self) -> Option<NonNull<GoaAccount>>;
    fn as_goa_mail(self) -> Option<NonNull<GoaMail>>;
    fn as_goa_oauth_based(self) -> Option<NonNull<GoaOAuthBased>>;
    fn as_goa_oauth2_based(self) -> Option<NonNull<GoaOAuth2Based>>;
    fn as_goa_password_based(self) -> Option<NonNull<GoaPasswordBased>>;
}

impl GoaObjectIface for NonNull<GoaObject> {
    fn as_goa_account(self) -> Option<NonNull<GoaAccount>> {
        let ptr = unsafe { goa_object_get_account(self.as_ptr()) };

        NonNull::new(ptr)
    }

    fn as_goa_mail(self) -> Option<NonNull<GoaMail>> {
        let ptr = unsafe { goa_object_get_mail(self.as_ptr()) };

        NonNull::new(ptr)
    }

    fn as_goa_oauth_based(self) -> Option<NonNull<GoaOAuthBased>> {
        let ptr = unsafe { goa_object_get_oauth_based(self.as_ptr()) };

        NonNull::new(ptr)
    }

    fn as_goa_oauth2_based(self) -> Option<NonNull<GoaOAuth2Based>> {
        let ptr = unsafe { goa_object_get_oauth2_based(self.as_ptr()) };

        NonNull::new(ptr)
    }

    fn as_goa_password_based(self) -> Option<NonNull<GoaPasswordBased>> {
        let ptr = unsafe { goa_object_get_password_based(self.as_ptr()) };

        NonNull::new(ptr)
    }
}
