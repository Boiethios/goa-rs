/// An error while retrieving the client.
#[derive(Debug, Clone)]
pub enum Error {
    /// The client cannot be retrieved.
    ClientDisconnected,
    /// The client pointer was null.
    ClientNull,
    /// A GNOME error.
    ClientNativeError(String),
}

impl std::fmt::Display for Error {
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Error::ClientNativeError(s) => write!(fmt, "goa::Error: GNOME error: '{}'", s),
            Error::ClientDisconnected => write!(fmt, "goa::Error: Task disconnected"),
            Error::ClientNull => write!(fmt, "goa::Error: Client pointer is null"),
        }
    }
}

impl std::error::Error for Error {}
