use crate::ffi::{
    goa_mail_get_email_address, goa_mail_get_imap_accept_ssl_errors, goa_mail_get_imap_host,
    goa_mail_get_imap_supported, goa_mail_get_imap_use_ssl, goa_mail_get_imap_use_tls,
    goa_mail_get_imap_user_name, goa_mail_get_name, goa_mail_get_smtp_accept_ssl_errors,
    goa_mail_get_smtp_auth_login, goa_mail_get_smtp_auth_plain, goa_mail_get_smtp_auth_xoauth2,
    goa_mail_get_smtp_host, goa_mail_get_smtp_supported, goa_mail_get_smtp_use_auth,
    goa_mail_get_smtp_use_ssl, goa_mail_get_smtp_use_tls, goa_mail_get_smtp_user_name, GoaMail,
    GoaObject, GoaObjectIface,
};
use crate::utils::char_ptr_to_string;
use glib::glib_sys::GTRUE;
use std::{fmt, marker::PhantomData, ptr::NonNull};

pub struct Mail<'a> {
    mail_ptr: NonNull<GoaMail>,
    _marker: PhantomData<&'a ()>,
}

impl Mail<'_> {
    /// Returns `None` if `object` is not a valid GoaAccount.
    pub(crate) fn new(object: NonNull<GoaObject>) -> Option<Self> {
        object.as_goa_mail().map(|mail_ptr| Mail {
            mail_ptr,
            _marker: PhantomData,
        })
    }

    #[inline]
    pub fn email_address(&self) -> Option<String> {
        unsafe { char_ptr_to_string(goa_mail_get_email_address(self.mail_ptr.as_ptr())) }
    }

    #[inline]
    pub fn name(&self) -> Option<String> {
        unsafe { char_ptr_to_string(goa_mail_get_name(self.mail_ptr.as_ptr())) }
    }

    #[inline]
    pub fn imap_host(&self) -> Option<String> {
        unsafe { char_ptr_to_string(goa_mail_get_imap_host(self.mail_ptr.as_ptr())) }
    }

    #[inline]
    pub fn imap_user_name(&self) -> Option<String> {
        unsafe { char_ptr_to_string(goa_mail_get_imap_user_name(self.mail_ptr.as_ptr())) }
    }

    #[inline]
    pub fn imap_supported(&self) -> bool {
        let b = unsafe { goa_mail_get_imap_supported(self.mail_ptr.as_ptr()) };

        b == GTRUE
    }

    #[inline]
    pub fn imap_accept_ssl_errors(&self) -> bool {
        let b = unsafe { goa_mail_get_imap_accept_ssl_errors(self.mail_ptr.as_ptr()) };

        b == GTRUE
    }

    #[inline]
    pub fn imap_use_ssl(&self) -> bool {
        let b = unsafe { goa_mail_get_imap_use_ssl(self.mail_ptr.as_ptr()) };

        b == GTRUE
    }

    #[inline]
    pub fn imap_use_tls(&self) -> bool {
        let b = unsafe { goa_mail_get_imap_use_tls(self.mail_ptr.as_ptr()) };

        b == GTRUE
    }

    #[inline]
    pub fn smtp_host(&self) -> Option<String> {
        unsafe { char_ptr_to_string(goa_mail_get_smtp_host(self.mail_ptr.as_ptr())) }
    }

    #[inline]
    pub fn smtp_user_name(&self) -> Option<String> {
        unsafe { char_ptr_to_string(goa_mail_get_smtp_user_name(self.mail_ptr.as_ptr())) }
    }

    #[inline]
    pub fn smtp_supported(&self) -> bool {
        let b = unsafe { goa_mail_get_smtp_supported(self.mail_ptr.as_ptr()) };

        b == GTRUE
    }

    #[inline]
    pub fn smtp_accept_ssl_errors(&self) -> bool {
        let b = unsafe { goa_mail_get_smtp_accept_ssl_errors(self.mail_ptr.as_ptr()) };

        b == GTRUE
    }

    #[inline]
    pub fn smtp_use_auth(&self) -> bool {
        let b = unsafe { goa_mail_get_smtp_use_auth(self.mail_ptr.as_ptr()) };

        b == GTRUE
    }

    #[inline]
    pub fn smtp_auth_login(&self) -> bool {
        let b = unsafe { goa_mail_get_smtp_auth_login(self.mail_ptr.as_ptr()) };

        b == GTRUE
    }

    #[inline]
    pub fn smtp_auth_plain(&self) -> bool {
        let b = unsafe { goa_mail_get_smtp_auth_plain(self.mail_ptr.as_ptr()) };

        b == GTRUE
    }

    #[inline]
    pub fn smtp_auth_xoauth2(&self) -> bool {
        let b = unsafe { goa_mail_get_smtp_auth_xoauth2(self.mail_ptr.as_ptr()) };

        b == GTRUE
    }

    #[inline]
    pub fn smtp_use_ssl(&self) -> bool {
        let b = unsafe { goa_mail_get_smtp_use_ssl(self.mail_ptr.as_ptr()) };

        b == GTRUE
    }

    #[inline]
    pub fn smtp_use_tls(&self) -> bool {
        let b = unsafe { goa_mail_get_smtp_use_tls(self.mail_ptr.as_ptr()) };

        b == GTRUE
    }
}

impl fmt::Debug for Mail<'_> {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        f.debug_struct("Mail")
            .field("email_address", &self.email_address())
            .field("name", &self.name())
            .field("imap_host", &self.imap_host())
            .field("imap_user_name", &self.imap_user_name())
            .field("imap_supported", &self.imap_supported())
            .field("imap_accept_ssl_errors", &self.imap_accept_ssl_errors())
            .field("imap_use_ssl", &self.imap_use_ssl())
            .field("imap_use_tls", &self.imap_use_tls())
            .field("smtp_host", &self.smtp_host())
            .field("smtp_user_name", &self.smtp_user_name())
            .field("smtp_supported", &self.smtp_supported())
            .field("smtp_accept_ssl_errors", &self.smtp_accept_ssl_errors())
            .field("smtp_use_auth", &self.smtp_use_auth())
            .field("smtp_auth_login", &self.smtp_auth_login())
            .field("smtp_auth_plain", &self.smtp_auth_plain())
            .field("smtp_auth_xoauth2", &self.smtp_auth_xoauth2())
            .field("smtp_use_ssl", &self.smtp_use_ssl())
            .field("smtp_use_tls", &self.smtp_use_tls())
            .finish()
    }
}
