use crate::ffi::{GoaObject, GoaObjectIface, GoaPasswordBased};
use std::{fmt, marker::PhantomData, ptr::NonNull};

use crate::ffi::goa_password_based_call_get_password_sync;

pub struct PasswordBased<'a> {
    auth_ptr: NonNull<GoaPasswordBased>,
    _marker: PhantomData<&'a ()>,
}

impl PasswordBased<'_> {
    pub(crate) fn new(object: NonNull<GoaObject>) -> Option<Self> {
        object
            .as_goa_password_based()
            .map(|auth_ptr| PasswordBased {
                auth_ptr,
                _marker: PhantomData,
            })
    }

    //TODO: remove those 3 funcs, sync version only temp.
    //TODO: pub fn imap_password(&self) -> impl Future<Output = Option<String>>
    pub fn imap_password(&self) -> Option<String> {
        self.password("imap-password")
    }

    pub fn smtp_password(&self) -> Option<String> {
        self.password("smtp-password")
    }

    fn password(&self, arg_id: &str) -> Option<String> {
        let mut out_password = std::ptr::null_mut();
        let mut error = std::ptr::null_mut();
        let arg_id = std::ffi::CString::new(arg_id).unwrap();

        let succeded = unsafe {
            goa_password_based_call_get_password_sync(
                self.auth_ptr.as_ptr(),
                arg_id.as_ptr(),
                &mut out_password as *mut _,
                std::ptr::null_mut(),
                &mut error as *mut _,
            )
        };
        if succeded == glib::glib_sys::GTRUE {
            unsafe {
                Some(
                    std::ffi::CStr::from_ptr(out_password)
                        .to_string_lossy()
                        .into_owned(),
                )
            }
        } else {
            None
        }
    }
}

impl fmt::Debug for PasswordBased<'_> {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        f.debug_struct("PasswordBased")
            .field("auth_ptr", &self.auth_ptr)
            .finish()
    }
}
