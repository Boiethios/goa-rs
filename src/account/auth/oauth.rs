use crate::ffi::{GoaOAuthBased, GoaObject, GoaObjectIface};
use std::{fmt, marker::PhantomData, ptr::NonNull};

pub struct OAuthBased<'a> {
    auth_ptr: NonNull<GoaOAuthBased>,
    _marker: PhantomData<&'a ()>,
}

impl OAuthBased<'_> {
    pub(crate) fn new(object: NonNull<GoaObject>) -> Option<Self> {
        object.as_goa_oauth_based().map(|auth_ptr| OAuthBased {
            auth_ptr,
            _marker: PhantomData,
        })
    }
}

impl fmt::Debug for OAuthBased<'_> {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        f.debug_struct("OAuthBased")
            .field("auth_ptr", &self.auth_ptr)
            .finish()
    }
}
