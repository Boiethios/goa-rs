use crate::ffi::{GoaOAuth2Based, GoaObject, GoaObjectIface};
use std::{fmt, marker::PhantomData, ptr::NonNull};

pub struct OAuth2Based<'a> {
    auth_ptr: NonNull<GoaOAuth2Based>,
    _marker: PhantomData<&'a ()>,
}

impl OAuth2Based<'_> {
    pub(crate) fn new(object: NonNull<GoaObject>) -> Option<Self> {
        object.as_goa_oauth2_based().map(|auth_ptr| OAuth2Based {
            auth_ptr,
            _marker: PhantomData,
        })
    }
}

impl fmt::Debug for OAuth2Based<'_> {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        f.debug_struct("OAuth2Based")
            .field("auth_ptr", &self.auth_ptr)
            .finish()
    }
}
