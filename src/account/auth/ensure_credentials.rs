use std::{
    ffi::{c_void, CStr},
    future::Future,
    marker::PhantomData,
    pin::Pin,
    ptr::null_mut,
    sync::mpsc::{channel, Receiver, Sender, TryRecvError},
    task::{Context, Poll},
};

pub struct FutureEnsureCredentials<'a> {
    receiver: Receiver<Result<bool, Error>>,
    _marker: PhantomData<&'a ()>,
}