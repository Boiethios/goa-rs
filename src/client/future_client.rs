use crate::ffi::{goa_client_new, goa_client_new_finish, GAsyncResult, GoaClient};
use crate::{Client, Error};
use glib::{glib_sys::GError, gobject_sys::GObject};

use std::{
    ffi::{c_void, CStr},
    future::Future,
    marker::PhantomData,
    pin::Pin,
    ptr::null_mut,
    sync::mpsc::{channel, Receiver, Sender, TryRecvError},
    task::{Context, Poll},
};

pub struct FutureClient<'a> {
    receiver: Receiver<Result<*mut GoaClient, Error>>,
    _marker: PhantomData<&'a ()>,
}

impl<'a> Future for FutureClient<'a> {
    type Output = Result<Client<'a>, Error>;

    fn poll(self: Pin<&mut Self>, cx: &mut Context) -> Poll<Self::Output> {
        println!("Polling…");

        let result = match self.receiver.try_recv() {
            Ok(Ok(client_ptr)) => Poll::Ready(Client::new(client_ptr)),
            Ok(Err(e)) => Poll::Ready(Err(e)),
            Err(TryRecvError::Empty) => {
                std::thread::sleep(std::time::Duration::from_millis(10));
                cx.waker().wake_by_ref();
                Poll::Pending
            }
            Err(TryRecvError::Disconnected) => Poll::Ready(Err(Error::ClientDisconnected)),
        };

        println!("Polling result: {:?}", result);

        result
    }
}

impl FutureClient<'_> {
    /// Sets the callback before returning the future client.
    pub fn new() -> Self {
        let (sender, receiver) = channel();
        let sender = Box::into_raw(Box::new(sender));

        unsafe {
            goa_client_new(null_mut(), on_client_ready, sender as *mut c_void);
        }

        FutureClient {
            receiver,
            _marker: Default::default(),
        }
    }
}

/// When the native client is ready, sets the `client` field of `FutureClient`.
#[no_mangle]
unsafe extern "C" fn on_client_ready(
    _source: *mut GObject,
    result: *mut GAsyncResult,
    sender: *mut c_void,
) {
    let sender = sender as *mut Sender<Result<*mut GoaClient, Error>>;
    let sender = Box::from_raw(sender);
    let mut error: *mut GError = null_mut();
    let native = goa_client_new_finish(result, &mut error as *mut _);

    let msg = if error.is_null() {
        Ok(native)
    } else {
        let msg = CStr::from_ptr((*error).message)
            .to_string_lossy()
            .into_owned();

        Err(Error::ClientNativeError(msg))
    };

    // Cannot do error handling
    if let Err(e) = sender.send(msg) {
        eprintln!("ERROR: cannot send the message to set the client: {}", e);
    }
}
