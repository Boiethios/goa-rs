pub(crate) mod future_client;

use crate::ffi::{goa_client_get_accounts, GListIter, GoaClient};
use crate::{Account, Error};
use glib::gobject_sys::{g_clear_object, GObject};
use std::{marker::PhantomData, ptr::NonNull};

#[derive(Debug)]
pub struct Client<'a> {
    client_ptr: NonNull<GoaClient>,
    _marker: PhantomData<&'a ()>,
}

impl Client<'_> {
    pub(crate) fn new(client_ptr: *mut GoaClient) -> Result<Self, Error> {
        NonNull::new(client_ptr)
            .ok_or(Error::ClientNull)
            .map(|client_ptr| Client {
                client_ptr,
                _marker: PhantomData,
            })
    }

    /// Get the user's accounts.
    pub fn accounts(&self) -> Vec<Account> {
        let accounts_list = unsafe { goa_client_get_accounts(self.client_ptr.as_ptr()) };

        GListIter::new(accounts_list)
            .filter_map(Account::new)
            .collect()
    }
}

impl Drop for Client<'_> {
    fn drop(&mut self) {
        unsafe {
            g_clear_object(&mut self.client_ptr as *mut _ as *mut *mut GObject);
        }
    }
}
