use std::{ os::raw::c_char, ffi::CStr};

pub unsafe fn char_ptr_to_string(s: *const c_char) -> Option<String> {
    if s.is_null() {
        None
    } else {
        let s = CStr::from_ptr(s).to_string_lossy().into_owned();

        Some(s)
    }
}